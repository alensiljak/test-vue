/*
    Dev configuration
*/
import vue from 'rollup-plugin-vue'
// import babel from 'rollup-plugin-buble';
// import resolve from 'rollup-plugin-node-resolve';
// import commonjs from 'rollup-plugin-commonjs';

export default {
    input: 'src/App.vue',
    //input: 'src/main.js',
    output: {
        file: 'dist/app.js',
        //format: 'cjs'
        format: 'iife',
        //format: 'esm',
        //format: 'amd'
        //format: 'umd',
        name: 'my_module_name',
        //format: 'es'
        //format: 'system'
    },
    plugins: [
        vue({ compileTemplate: true }) /* VuePluginOptions */
        // resolve({
        //     jsnext: true,
        //     main: true,
        //     browser: true,
        // }),
        // commonjs(),
        // babel()
    ]
}