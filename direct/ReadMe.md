# Direct Vue

## httpVueLoader

Also try [HttpVueLoader](https://github.com/FranckFreiburger/http-vue-loader). See [example](https://github.com/kafkaca/vue-without-webpack).

## References

- https://jsmodules.io/
- https://vuejs.org/v2/guide/
