const webpack = require('webpack');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const config = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: [
      '*',
      '.js',
      '.vue',
      '.json'
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  devServer: {
    //noInfo: false,
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 32000
  }
}

module.exports = config;